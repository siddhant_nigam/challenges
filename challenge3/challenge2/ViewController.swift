//
//  ViewController.swift
//  challenge2
//
//  Created by LiveLike on 08/03/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
           super.viewDidLoad()
    }
   
    @IBAction func birdButtonPressed(_ sender: UIButton) {
        imageView.image = #imageLiteral(resourceName: "bird")
    }
    
    @IBAction func dogButtonPressed(_ sender: UIButton) {
        imageView.image = #imageLiteral(resourceName: "dog")
    }
    
    @IBAction func catButtonPressed(_ sender: UIButton) {
        imageView.image = #imageLiteral(resourceName: "cat")
    }
}


