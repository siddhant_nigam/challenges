//
//  AppDelegate.swift
//  fit_demo_app
//
//  Created by LiveLike on 14/04/22.
//

import UIKit
import EngagementSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var engagementSDK: EngagementSDK!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var config = EngagementSDKConfig(clientID: "vGgUtbZTQWW6C6ROKSqRAO9wdrZaGffXEzYIAxwQ")
            engagementSDK = EngagementSDK(config: config)
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

