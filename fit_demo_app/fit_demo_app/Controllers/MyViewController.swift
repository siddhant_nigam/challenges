import UIKit
import EngagementSDK

class MyViewController: UIViewController {

    
    @IBOutlet weak var widgetView: UIView!
   
    
var engagementSDK: EngagementSDK!

    //Creating the Widget and Chat ViewControllers
    private var contentSession: ContentSession!

    private var timelineVC:InteractiveWidgetTimelineViewController!
//    init(contentSession: ContentSession) {
//        self.timelineVC = InteractiveWidgetTimelineViewController(contentSession: contentSession)
//           super.init(nibName: nil, bundle: nil)
//     }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
    //Holding a reference to the session to keep it valid
//    var session: ContentSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var sdkConfig = EngagementSDKConfig(clientID: "vGgUtbZTQWW6C6ROKSqRAO9wdrZaGffXEzYIAxwQ")
        engagementSDK = EngagementSDK(config: sdkConfig)
        let config = SessionConfiguration(programID: "285e4d20-b60c-415d-9624-394646b4471a")
        contentSession = engagementSDK.contentSession(config: config)
        self.timelineVC = InteractiveWidgetTimelineViewController(contentSession: contentSession)
        
        
        // Add timelineVC to layout
           addChild(timelineVC)
               timelineVC.didMove(toParent: self)
           timelineVC.view.translatesAutoresizingMaskIntoConstraints = false
        widgetView.addSubview(timelineVC.view)
        
        NSLayoutConstraint.activate([
            timelineVC.view.topAnchor.constraint(equalTo: widgetView.topAnchor),
              timelineVC.view.leadingAnchor.constraint(equalTo: widgetView.leadingAnchor),
              timelineVC.view.trailingAnchor.constraint(equalTo: widgetView.trailingAnchor),
              timelineVC.view.bottomAnchor.constraint(equalTo: widgetView.bottomAnchor)
            ])
      
        

       
        
    }

    //Ending a session
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.contentSession.close()
    }

    @IBAction func chatButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToChatView", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "goToChatView"{
            let destinationVC = segue.destination as!chatViewController
        }
    }
}

