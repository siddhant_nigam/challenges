//
//  NewViewController.swift
//  fit_demo_app
//
//  Created by LiveLike on 19/04/22.
//

import UIKit
import EngagementSDK

class chatViewController: UIViewController {
    
    @IBOutlet weak var chatView: UIView!
    var engagementSDK: EngagementSDK!
    let chatViewController = ChatViewController()
    var session: ContentSession?
    var chatSession: ChatSession!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var sdkConfig = EngagementSDKConfig(clientID: "vGgUtbZTQWW6C6ROKSqRAO9wdrZaGffXEzYIAxwQ")
        engagementSDK = EngagementSDK(config: sdkConfig)
//        engagementSDK.createChatRoom(title:"New Room", visibility: .everyone) { result in
//                switch result {
//                case let .success(chatRoomID):
//                    print("New Room Created \(chatRoomID)")
//                case let .failure(error):
//                    print("Failed creating a room \(error)")
//                }
//             }
        engagementSDK.getChatRoomInfo(roomID: "26bca547-0110-4a99-a7c7-aae2153ec1e6") { result in
              switch result {
              case let .success(chatInfo):
                  print("Chat Room Title: \(chatInfo.title ?? "No Title")")
              case let .failure(error):
                  print("Error: \(error.localizedDescription)")
              }
            }
        let Config = ChatSessionConfig(roomID: "26bca547-0110-4a99-a7c7-aae2153ec1e6")
            engagementSDK.connectChatRoom(config: Config) { [weak self] result in
              guard let self = self else { return }
                switch result {
                case .success(let chatSession):
                self.chatSession = chatSession
                
                // if you choose to show the `ChatSession` to the user
                // you have to set it on the `ChatViewController`
                self.chatViewController.setChatSession(chatSession)
                case.failure(let error): break
                }
            }
        
        // Adding chatViewController as a child view controller
        addChild(chatViewController)
        chatView.addSubview(chatViewController.view)
        chatViewController.didMove(toParent: self)
        
        
        // Applying constraints to fill the container
        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            chatViewController.view.bottomAnchor.constraint(equalTo: chatView.bottomAnchor),
            chatViewController.view.topAnchor.constraint(equalTo: chatView.topAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: chatView.trailingAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: chatView.leadingAnchor)
            ])
        //Creating a Content Session
        let config = SessionConfiguration(programID: "285e4d20-b60c-415d-9624-394646b4471a")
        session = engagementSDK.contentSession(config: config)
        // Do any additional setup after loading the view.
        chatViewController.session = session
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.session?.close()
    }

    

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
